#!/bin/bash
MY_NODE_VERSION=12
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../" >/dev/null 2>&1 && pwd)"
cd "$DIR" || return
echo $DIR
npm install
source /usr/share/nvm/init-nvm.sh > /dev/null 2>&1
if $(command -v nvm >/dev/null 2>&1); then
	echo "Using NVM to select NodeJS $MY_NODE_VERSION"
	run_cmd="nvm use $MY_NODE_VERSION && "
fi
run_cmd+="npm install && "
if $(command pm2 -v >/dev/null 2>&1); then
	echo "Using PM2 to manage process"
	# run_cmd+="pm2 start clemette.js --restart-delay 2000 --name clemette"
	run_cmd+="pm2 install . --restart-delay 10000"
else
	echo "Running using node directly"
	run_cmd+="node clemette.js"
fi
eval $run_cmd
pm2 save --force
