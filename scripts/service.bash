#!/bin/bash
set -e
MY_NODE_VERSION=12
# echo "DIR=$DIR"
new_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
echo "new_dir=$new_dir"
cd "$new_dir/../"
# pwd
# pushd $new_dir/../
npm install
# source /usr/share/nvm/init-nvm.sh > /dev/null 2>&1
# if $(command -v nvm >/dev/null 2>&1); then
	# echo "Using NVM to select NodeJS $MY_NODE_VERSION"
	# run_cmd="nvm use $MY_NODE_VERSION && "
# fi
node app/bot.js
