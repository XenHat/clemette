REM You really should be using `npm start` instead
cd /d %~dp0/../
npm install
pm2 start app/index.js --restart-delay 10000 --name clemette
