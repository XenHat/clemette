// FIXME: Remove all hard-coded IDs and use config file
// TODO: Use sequalize to store config?
// TODO: Move features to modules

/* eslint-disable camelcase */
'use strict'

const fs = require('node:fs')
const path = require('node:path')
const _ = require('lodash')
const Twitch = require('twitch.tv-api')
const ISO6391 = require('iso-639-1')

require('dotenv').config()

// var jsonPath = path.join(__dirname, '..', 'storage', 'settings.json');
// var configFile = fs.readFileSync(jsonPath, 'utf8');
// TODO: move config file handling to a module
// TODO: Move to new const { variable,names,here} = require(filename_here)
const configFile = path.join(__dirname, 'storage', 'config.json')
const jokesFile = path.join(__dirname, 'assets', 'dadjokes.json')
let configData = {}
let jokesData = {}

// Global Variables
// const debug = process.env.NODE_ENV !== 'production';
const debugPings = false
const EnablePings = true
// TODO: use per-user cooldown by pairing id and current time + timeout_seconds
let hasCooledDown = true
let twitch
const twitchData = {} // new Object();
let oldData = {} // for ping tracking
twitchData.streams = [] // new Array();
oldData.streams = [] // new Array();

const Logger = require('./modules/logger.js')

if (process.env.NODE_ENV !== 'production') {
	Logger.con('Including LongJohn')
	require('longjohn')
}

reloadConfig()

const cwd = process.cwd()
console.log(`Working directory: ${cwd}`)

// const Cluster = require('discord-hybrid-sharding')
const Discord = require('discord.js')
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');
const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.MessageContent,
		GatewayIntentBits.GuildMembers,
	],
	partials: ['MESSAGE', 'CHANNEL', 'REACTION']
})
client.commands = new Collection();
const foldersPath = path.join(__dirname, 'commands');
const commandFolders = fs.readdirSync(foldersPath);

for (const folder of commandFolders) {
	const commandsPath = path.join(foldersPath, folder);
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file);
		const command = require(filePath);
		// Set a new item in the Collection with the key as the command name and the value as the exported module
		if ('data' in command && 'execute' in command) {
			client.commands.set(command.data.name, command);
		} else {
			console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

// const usev13 = true // When you do not use v13, turn this value to false
// client.cluster = new Cluster.Client(client, usev13) // Init the CLient & So we can also access broadcastEval...


//   const hackGotFirstRemoveEmojiMessage = false
// TODO: use per-user cooldown by pairing id and current time + timeout_seconds
// let hasCooledDown = true
function getGuildConfig (guildID, what) {
	const lookup = {}
	// TODO: Replace with a filter function?
	configData.guilds.forEach(function (el) {
		lookup[el.id] = el[what]
	})
	return lookup[guildID]
}
function loadConfig () {
	twitch = new Twitch({
		id: process.env.TWITCH_API_ID,
		secret: process.env.TWITCH_API_SECRET,
	});

	if (fs.existsSync(configFile) === false) {
		Logger.con(`Config file ${configFile} does not exist!`)
		return null
	} else {
		try {
			let buff = fs.readFileSync(configFile, 'utf8')
			configData = JSON.parse(buff)
			if (fs.existsSync(jokesFile)) {
				try {
					buff = fs.readFileSync(jokesFile, 'utf8')
					jokesData = JSON.parse(buff)
				} catch (e) {
					Logger.con(`ERROR: ${e}`)
				}
			} else {
				Logger.con(`Jokes file ${jokesFile} does not exist!`)
			}
		} catch (e) {
			Logger.con(`ERROR: ${e}`)
			return null
		}
	}
}
// function writeConfig() {
//  return writeConfigToFile(configData);
// }
async function reloadConfig () {
	await loadConfig()
}
function getRandomInt (min, max) {
	min = Math.ceil(min)
	max = Math.floor(max)
	return Math.floor(Math.random() * (max - min + 1)) + min
}
const getDadJoke = async function (message, OnlyAdultJokes) {
	if (hasCooledDown) {
		hasCooledDown = false
		const pgJokes = jokesData.pgJokes
		const adultJokes = jokesData.adultJokes
		let jokes = []
		if (OnlyAdultJokes) {
			jokes = adultJokes
		} else {
			jokes = pgJokes
			if (message.channel.nsfw === true) {
				jokes.concat(adultJokes)
			}
		}
		const joke = `${jokes[Math.floor(getRandomInt(0, jokes.length))]}`
		const timeout = (Math.round(joke.length * 4.166666666) * 10) // 50 wpm => 250 cpm => 4.1666¯ characters per second
		Logger.con(`This should take ${timeout}ms to type`)
		message.channel.startTyping()
		setTimeout(function () {
			message.channel.stopTyping()
			message.channel.send(joke)
		}, timeout)
		process.nextTick(() => {
			hasCooledDown = true
		}, 5000)
	}
}
// TODO: Define commands as a class or something, and print their help property when calling the help command
async function processChatCommand (prefix, message, command) {
	if (command !== undefined) {
		// Temporary fix until I re-write the configuration overlaying for global/guild
		// const isSuper = hasModeratorRights(message);
		if (command === 'ping') {
			message.reply(`Pong! API Latency is ${Math.round(client.ws.ping)}ms.`)
				.then(msg => {
					setTimeout(() => msg.delete(), 10000)
				})
				.catch(/* Your Error handling if the Message isn't returned, sent, etc. */)
			// m.delete(60000);
			// TODO: pad with string.padStart(newlength) and padEnd(newlength)
			// } else if (command === 'help') {
			//  await message.channel.send(
			//    '**Available commands:**\n' +
			//              `\`${prefix}ping\` + (`*Latency test*`).padStart(20) +
			//              `\`${prefix}dadjoke[+]\`*Do you really need help about this?*\n`
			//  )
		} else if (command === 'dadjoke') {
			getDadJoke(message)
		} else if (command === 'dadjoke+') {
			getDadJoke(message, true)
			//   } else if (command === 'setuproles') {
			// setupRolesReactions(message.id);
		} else if (command === 'embed') {
			// inside a command, event listener, etc.
			const exampleEmbed = new Discord.MessageEmbed()
				.setColor('#0099ff') // role color maybe?
				.setTitle("OwO, what's this?!")
				.setURL('https://twitch.tv/xenhat')
				.setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
				.setDescription('Some description here')
				.setThumbnail('https://i.imgur.com/wSTFkRM.png')
				.addFields(
					{ name: 'Regular field title', value: 'Some value here' },
					{ name: '\u200B', value: '\u200B' },
					{ name: 'Inline field title', value: 'Some value here', inline: true },
					{ name: 'Inline field title', value: 'Some value here', inline: true }
				)
				.addField('Inline field title', 'Some value here', true)
				.setImage('https://i.imgur.com/wSTFkRM.png')
				.setTimestamp()
				.setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png')
			message.channel.send(exampleEmbed)
		} else {
			message.react('❔')
			return
		}
		setTimeout(function () {
			message.delete()
		}, 3000)
	}
}
// Utility function to keep the code clean across Discord.JS versions
//  function getChannelByID (channel_id) {
//    // Discord.JS >= 12
//    return client.channels.cache.get(channel_id)
//  }
// -------------------------------------------------------------------------//

function delay (ms) {
	return new Promise((resolve) => setTimeout(resolve, ms))
}

// Global functions depending on other things above
async function startIdleLoop () {
	delay(1000)
	await refreshTwitchData()
	delay(5000)
	setInterval(async function () {
		await refreshTwitchData()
		await buildNotificationsForPings(false)
	}, 15000)
}

client.on('ready', () => {
	Logger.con(
		`Client ready. Connected to ${client.guilds.cache.size} guilds.`
	)
	startIdleLoop()
	client.user.setPresence({
		game: { name: 'with 🔥' },
		status: 'online'
	})
})
// React to welcome message (which can be existing before the bot has started)
//   client.on('raw', (packet) => {
//     const message_type = packet.t
//     if (
//       message_type === 'MESSAGE_REACTION_ADD' ||
//             message_type === 'MESSAGE_REACTION_REMOVE'
//     ) {
//       if (packet.d.user_id !== client.id) {
//         const message_id = packet.d.message_id
//         if (
//           message_id === '712064541395255329' ||
//                     message_id === '712063964338716753'
//         ) {
//           //   Logger.con(JSON.stringify(packet, '\n', 2));
//           Logger.con(`Message ID: ${packet.d.message_id}`)
//           const channel = getChannelByID(packet.d.channel_id)
//           // Cached messages will fire 'messageReactionAdd' and 'messageReactionAdd', but uncached ones will not, so we need to fire it ourselves then.
//           // FIXME: MESSAGE_REACTION_REMOVE seems to fail the cache check, and consequently does not trigger. Workaround: see 'hackGotFirstRemoveEmojiMessage' usage below
//           if (
//             (message_type === 'MESSAGE_REACTION_ADD' ||
//                             hackGotFirstRemoveEmojiMessage) &&
//                         channel.messages.has(packet.d.message_id)
//           ) {
//             return
//           }
//           if (
//             !hackGotFirstRemoveEmojiMessage &&
//                         message_type === 'MESSAGE_REACTION_REMOVE'
//           ) {
//             hackGotFirstRemoveEmojiMessage = true
//           }
//           // channel.fetchMessage(message_id).then((guild, message) => {
//           channel.messages.fetch(message_id).then((guild, reaction_msg) => {
//             //   Logger.con("Fetching...");
//             //   let reaction_user = guild.Logger.con(`User: ${message.client}`);
//             client.emit(
//               message_type === 'MESSAGE_REACTION_ADD'
//                 ? 'messageReactionAdd'
//                 : 'messageReactionRemove',
//               reaction_msg.reactions.get(
//                 [packet.d.emoji.name, packet.d.emoji.id]
//                   .filter(Boolean)
//                   .join(':')
//               ),
//             //   message.username
//             )
//           })
//         }
//       }
//     }
//   })
client.on('message', async (message) => {
	if (!message.author.bot) {
		if (message.author.id === client.user.id) {
			return
		}
		if (configData.ignoredUsers.indexOf(message.author.id) > -1) {
			return
		}

		if (message.mentions.has(client.user)) {
			// personality.chat(message)
			message.react('❔')
		} else {
			const prefix = getGuildConfig(message.guild.id, 'prefix')
			if (message.content.charAt(0) === prefix) {
				const args = message.content.slice(1).trim().split(' ')
				await processChatCommand(prefix, message, args[0], args[1])
			}
		}
	}
})
// client.on('messageReactionAdd', async (messageReaction, user) => {
//     if (user.id !== client.user.id) {
//         handleReaction(messageReaction, user, true);
//     }
// });
// client.on('messageReactionRemove', async (messageReaction, user) => {
//     if (user.id !== client.user.id) {
//         handleReaction(messageReaction, user, false);
//     }
// });

// -----------------
// Twitch.TV functions
function newTwitchData (user) {
	return {
		name: user,
		live: false,
		game: 'N/A',
		title: 'N/A'
	}
}
const getTwitchApiReply = async function (user) {
	await twitch.getUser(user).then(function (reply) {
		if (reply) {
			let live = false
			if (
				reply.stream &&
				reply.stream.channel &&
				reply.stream.channel.name &&
				reply.stream.stream_type === 'live' &&
				reply.stream.channel.status &&
				!reply.stream.channel.private_video
			) {
				live = true
				debugLog(`Got reply for ${user}`)
			}
			// overwrite twitchData
			twitchData.streams.forEach(function (item) {
				if (item.name === user) {
					Logger.con(`USER: ${item.name}`)
					item.live = live
					if (live) {
						item.displayName = reply.stream.channel.display_name
						item.game = reply.stream.channel.game
						item.title = reply.stream.channel.status
						item.createdAt = reply.stream.created_at
						item.logo = reply.stream.channel.logo
						// Hack!
						item.preview = reply.stream.preview.large
						// item.preview = `https://static-cdn.jtvnw.net/previews-ttv/live_user_${item.name}-1920x1080.jpg`;
						item.isPartner = reply.stream.channel.partner
						item.isMature = reply.stream.channel.mature
						item.viewers = reply.stream.viewers
						item.followers = reply.stream.channel.followers
						item.lang = ISO6391.getName(
							reply.stream.channel.broadcaster_language
						)
						item.followers = reply.stream.channel.followers
					} else {
						if (debugPings) {
							item.live = true
							item.game = 'Debug All The Things!'
							item.title = 'Test Title'
							// item.createdAt = '';
							item.logo =
								'http://1.bp.blogspot.com/_D_Z-D2tzi14/TBpOnhVqyAI/AAAAAAAADFU/8tfM4E_Z4pU/s400/responsibility12(alternate).png'
							item.preview = item.logo
							item.isPartner = 'No'
							item.isMature = 'No'
							item.viewers = '1'
							item.followers = '8001'
							item.lang = 'fr'
							item.followers = '9'
						}
					}
					debugLog(`Created Data for ${user}: ${JSON.stringify(item)}`)
				}
			})
		} else {
			debugLog('ERROR: Got null reply from twitch!')
		}
	})
	oldData = {}
	oldData = _.cloneDeep(twitchData)
}
/*  Okay so: Asynchronous functions comes in pairs:
a defined callback, and the function itself.
Most of the time, you will define a callback yourself,
but it is possible to use an imported or built-in function
such as Logger.con or util.log.

============================
Example from https://stackoverflow.com/a/19739852:

var myCallback = function(err, data) {
if (err) throw err; // Check for the error and throw if it exists.
Logger.con('got data: '+data); // Otherwise proceed as usual.
};
var usingItNow = function(callback) {
var myError = new Error('My custom error!');
callback(myError, 'get it?'); // I send my error as the first argument.
};

USAGE:

usingItNow(myCallback);
============================
*/
function twitchTest (user) {
	/*  TODO: Create single-user ping builder
and use it as callback for this function: i.e.
twitchTest(user,callback){
...
}
refreshTwitchData() {
...
twitchTest(item.name,buildPingSingle(result));
}
*/
	debugLog(` => ${user}`)
	const p = new Promise(() => getTwitchApiReply(user)) // <= Single user builder callback goes here
	return p
}
async function refreshTwitchData () {
	if (typeof(configData) !== "undefined") {
		if (typeof(configData.streams) !== "undefined") {
			debugLog('Requesting data from Twitch.tv API...') 
			configData.streams.forEach(async function (item) {
				await twitchTest(item.name)
			})
			// Sort the items now that we have the new data
			//    twitchData.streams.sort(function(a, b) {
			//        var nameA = a.name.toLowerCase(),
			//            nameB = b.name.toLowerCase();
			//        if (nameA < nameB) { //sort string ascending
			//            return -1;
			//        }
			//        if (nameA > nameB) {
			//            return 1;
			//        }
			//        return 0; //default return value (no sorting)
			//    });
			debugLog('Refreshed Twitch Data')
		}
	}
}
Array.prototype.getIndexOf = function (el) {
	const arr = this
	for (let i = 0; i < arr.length; i++) {
		if (arr[i].name === el) {
			return i
		}
	}
	return -1
}

function loadAllStreamsToWatch () {
	debugLog('Loading streams...')
	oldData = _.cloneDeep(twitchData)
	twitchData.streams = []
	debugLog(`Type of configData: ${typeof configData}`)
	debugLog(`streams:${JSON.stringify(configData.streams)}`)
	if (configData.streams === undefined) {
		console.error('ConfigData format is wrong!')
		return
	}
	configData.streams.forEach(function (s) {
		const name = s.name.toLowerCase()
		if (!(name in twitchData.streams)) {
			const obj = newTwitchData(name)
			twitchData.streams.push(obj)
		} else {
			debugLog('WTF?!')
		}
	})
	if (configData.liveStreams === undefined) {
		configData.liveStreams = []
	} else {
		configData.liveStreams = deduplicateArray(configData.liveStreams)
	}
	debugLog('Finished loading streams')
	// writeConfig()
}

function deduplicateArray (myArray) {
	myArray = myArray.filter(function (elem, pos) {
		return myArray.indexOf(elem) === pos
	})
	return myArray
}

function debugLog (message) {
	// if (debug) {
	Logger.con(message)
	// }
}

function getEmbedObject (twDtStream, newLive, forPings) {
	debugLog(`building embed for ${twDtStream.displayName}`)
	const name = _.isEmpty(twDtStream.displayName)
		? twDtStream.name
		: twDtStream.displayName
	const embed = new Discord.MessageEmbed()
		.setTitle(
			`${name} ${forPings ? 'went' : 'is'} ${newLive ? 'LIVE!' : 'offline'}`
		)
		.setAuthor(name, twDtStream.logo)
		.setColor(13947850)
	// .setFooter(`${getClemitude()}`, client.user.avatarURL)
		.setFooter('Clem!', client.user.avatarURL)
		.setTimestamp(twDtStream.started_at)
	if (newLive) {
		embed.setURL(`https://twitch.tv/${twDtStream.name}`)
		embed.setDescription(
			`${_.isEmpty(twDtStream.title) ? 'N/A' : twDtStream.title}`
		)
		embed.setImage(twDtStream.preview)
		embed.setThumbnail(`${twDtStream.logo}`)
		embed.addField(
			'Playing',
			`${_.isEmpty(twDtStream.game) ? 'N/A' : twDtStream.game}`,
			true
		)
		embed.addField('Viewers', `${twDtStream.viewers}`, true)
		embed.addField(
			'Mature Stream',
			`${twDtStream.isMature ? 'Yes' : 'No'}`,
			true
		)
		embed.addField('Language', `${twDtStream.lang}`, true)
	}
	return embed
}
async function buildNotificationsForPings (forceping) {
	await setTimeout(function () {
		if (forceping !== undefined) {
			forceping = false
		}
		const previouslyLive = configData.liveStreams
		debugLog('Preparing pings...')
		for (let i = twitchData.streams.length - 1; i >= 0; i--) {
			const twDtStream = twitchData.streams[i]
			Logger.con(`HI THERE ${twDtStream}`)
			const wasLive = previouslyLive.indexOf(twDtStream.name) > -1
			const ChannelsToPing = []
			if (configData.streams.getIndexOf(twDtStream.name) < 0) {
				Logger.con(
					`Channels for ${twDtStream.name} not found in configData!`
				)
			} else {
				// exists in configData
				const streamPingChannelConfigData =
					configData.streams[configData.streams.getIndexOf(twDtStream.name)]
					.channels
				for (let i = streamPingChannelConfigData.length - 1; i >= 0; i--) {
					const c = streamPingChannelConfigData[i]
					// if (client.channels.has(c)) {
					if (client.channels.cache.get(c)) {
						ChannelsToPing.push(c)
					}
				}
				if (twDtStream.live === true) {
					debugLog(`=> ${twDtStream.name}`)
					debugLog(`Index: ${previouslyLive.indexOf(twDtStream.name)}`)
					if (wasLive === false) {
						if (forceping === true || wasLive === false) {
							for (let i = ChannelsToPing.length - 1; i >= 0; i--) {
								const ch = ChannelsToPing[i]
								const connectedChannel = client.channels.cache.get(ch)
								if (connectedChannel) {
									const perms = connectedChannel.permissionsFor(
										connectedChannel.guild.me
									)
									if (perms.bitfield & 0x00000800) {
										// SEND_MESSAGES
										debugLog(
											'Attempting Online Ping ' +
											ch.toString() +
											' (' +
											twDtStream.name +
											')'
										)
										if (forceping === true || EnablePings === true) {
											if (
												twDtStream.name !== 'warframe' ||
												twDtStream.title.includes('Nintendo') === false
											) {
												client.channels
													.cache.get(ch)
													.send(
														getEmbedObject(
															twDtStream,
															twDtStream.live === true,
															true
														)
													)
												Logger.con(`Pinging ${ch} for ${twDtStream.name}`)
											}
										} else {
											debugLog(
												`Would ping ${ch.toString()} (${twDtStream.name
												}) but prevented by checks`
											)
										}
									} else {
										Logger.con(
											`Permission denied to send messages in ${connectedChannel.name}`
										)
									}
								} else {
									debugLog(`Cannot send ping to ${ch}! (Not connected)`)
								}
							}
							if (previouslyLive.indexOf(twDtStream) === -1) {
								debugLog(`Adding ${twDtStream.name} to list of live streams`)
								configData.liveStreams.push(twDtStream.name)
							}
						} else {
							debugLog('Already pinged; skipping')
						}
					}
				} else if (wasLive === true && twDtStream.live === false) {
					// TODO: Re-use logic above and change message if not live instead of this block of code
					// Currently buggy; creates pings for live channels after removing from list.
					// debugLog('Not live :(');
					// if (wasLive === true) {
					//    if (forceping || pingedChannels.indexOf(twDtStream.name) > -1) {
					//        for (let i = ChannelsToPing.length - 1; i >= 0; i--) {
					//            let ch = ChannelsToPing[i];
					//            Logger.con('Offline Ping ' + ch.toString() + ' (' + twDtStream.name + ')');
					//            if (EnablePings === true) {
					//                client.channels.get(ch).send(getEmbedObject(twDtStream, twDtStream.live === true, true));
					//            }
					//        }
					//        const i = pingedChannels.indexOf(twDtStream.name);
					//        Logger.con('Removed from pinged list');
					//        if(!forceping){
					//            pingedChannels = pingedChannels.slice(i, i);
					//        }
					//    }
					// }
					// configData.liveStreams = deduplicateArray(previouslyLive);
					configData.liveStreams.splice(
						configData.liveStreams.indexOf(twDtStream.name),
						1
					)
				}
			}
		}
	}, 5000)
	// writeConfig()
}

function buildNotificationsForTwitch (message) {
	let hasStreams = false
	twitchData.streams.forEach(function (twDtStream) {
		const inew = configData.streams.getIndexOf(twDtStream.name)
		if (inew > -1) {
			configData.streams[inew].channels.forEach(function (c) {
				debugLog(`CurrentChannel: ${message.channel.id}`)
				debugLog(`loopingchannel: ${c}`)
				if (message.channel.id === c) {
					hasStreams = true
					const embed = getEmbedObject(
						twDtStream,
						twDtStream.live === true,
						false
					)
					if (twDtStream.live === true) {
						if (EnablePings === true) {
							message.channel.send(embed).catch(console.error)
						}
					}
				}
			})
		}
	})
	debugLog('configData.liveStreams' + JSON.stringify(configData.liveStreams))
	return hasStreams
}


client.on(Events.InteractionCreate, async interaction => {
	if (!interaction.isChatInputCommand()) return;

	const command = interaction.client.commands.get(interaction.commandName);

	if (!command) {
		console.error(`No command matching ${interaction.commandName} was found.`);
		return;
	}

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		if (interaction.replied || interaction.deferred) {
			await interaction.followUp({ content: 'There was an error while executing this command!', flags: MessageFlags.Ephemeral });
		} else {
			await interaction.reply({ content: 'There was an error while executing this command!', flags: MessageFlags.Ephemeral });
		}
	}
});

reloadConfig().then(loadAllStreamsToWatch())

const { REST, Routes } = require('discord.js');

const { clientId, guildId, token } = require(path.join(__dirname,'storage','config.json'));

const commands = [];
// Grab all the command folders from the commands directory you created earlier
//const foldersPath = path.join(__dirname, 'commands');
//const commandFolders = fs.readdirSync(foldersPath);

for (const folder of commandFolders) {
	// Grab all the command files from the commands directory you created earlier
	const commandsPath = path.join(foldersPath, folder);
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	// Grab the SlashCommandBuilder#toJSON() output of each command's data for deployment
	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file);
		const command = require(filePath);
		if ('data' in command && 'execute' in command) {
			commands.push(command.data.toJSON());
		} else {
			console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

// Construct and prepare an instance of the REST module
const rest = new REST().setToken(token);

// and deploy your commands!
(async () => {
	try {
		console.log(`Started refreshing ${commands.length} application (/) commands.`);

		// The put method is used to fully refresh all commands in the guild with the current set
		const data = await rest.put(
			Routes.applicationGuildCommands(clientId, guildId),
			{ body: commands },
		);

		console.log(`Successfully reloaded ${data.length} application (/) commands.`);
	} catch (error) {
		// And of course, make sure you catch and log any errors!
		console.error(error);
	}
})();

client.login(process.env.DISCORD_TOKEN)
