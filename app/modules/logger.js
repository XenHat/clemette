// const fs = require('fs')
// var log_file = fs.createWriteStream('logs/bot.log', { flags: 'a' });
class MyLogger {
  constructor (msg) {
    this.msg = msg
  }

  con (msg) {
    this._con(`CONLOG: ${msg}`)
  }

  _con (msg) {
    console.log(msg)
    // Do not log to file, as this triggers infinite loop for pm2
    // TODO: Detect pm2?
    // log_file.write(`${msg}\n`);
  }
}
module.exports = new MyLogger()
