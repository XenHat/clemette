// -----------------
// Personality
const log = require('./logger.js')
let hasCooledDown = true

function getClemitude () {
  const clemitudes = [
    'Clem!',
    'Grakata!',
    'Clem, clem, clem!',
    'Hmm, clem.',
    'Clem?'
  ]
  return clemitudes[Math.floor(Math.random() * clemitudes.length)]
}
function chat (message) {
  if (hasCooledDown) {
    log.con(`Handling '${message.content}`)
    const lci = message.content.toLowerCase()
    if (lci.includes('thank you')) {
      message.reply("You're welcome.")
    } else {
      message.channel.send(getClemitude())
    }
    hasCooledDown = false
    process.nextTick(() => {
      hasCooledDown = true
    }, 10000)
  }
}
exports.chat = chat
